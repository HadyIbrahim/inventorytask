namespace Invantory_View_To_MW.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;

    public partial class Item
    {

        [DisplayName("Item_ID")]
        [Key]
        public int I_ID { get; set; }
      

        [DisplayName("Item_Name")]
        [StringLength(50)]
        public string I_Name { get; set; }
        [DisplayName("Item_Code")]
        [StringLength(50)]
        public string I_Code { get; set; }
        [DisplayName("Item_Desc")]
        [StringLength(50)]
        public string I_Desc { get; set; }
        [DisplayName("Item_UserID")]
        public string User_ID { get; set; }
        [DisplayName("Item_date")]
        [Column(TypeName = "date")]
        public DateTime? I_Date { get; set; }
        [DisplayName("Item_Time")]
        [Column(TypeName = "Time")]
        public TimeSpan? I_Time { get; set; }
       
        public string token { get; set; }
      //public SelectList ItemList { get; set; }  
        //    public virtual ICollection<All_W_V_I> All_W_V_I { get; set; }
    }
}
