namespace Invantory_View_To_MW.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;
    using System.Web.WebPages.Html;

    public partial class All_W_V_I
    {
        public All_W_V_I()
        {
            this.Item = new List<Item>();

        }

        [Key]
        [DisplayName("transaction_ID")]
        public int A_ID { get; set; }
        [DisplayName("Warehouse_ID")]
        public int? W_ID { get; set; }
        [DisplayName("Vendor_ID")]
        public int? V_ID { get; set; }
        [DisplayName("Item_ID")]
        public int? I_ID { get; set; }
        [DisplayName("transaction_QTY")]
        public int? Qty { get; set; }
        [DisplayName("UserID")]
        public string User_ID { get; set; }
        [DisplayName("transaction_Date")]
        [Column(TypeName = "date")]
        public DateTime? A_Date { get; set; }
        [DisplayName("transaction_Time")]
        public TimeSpan? A_Time { get; set; }
        //public string SelectedCountryIso3 { get; set; }
        ////public IEnumerable<SelectListItem> Countries { get; set; }
        //public SelectList MobileList { get; set; }
      
        //public IEnumerable<Models.All_W_V_I> ItemLocations { get; set; }
        public  Vendor Vendor { get; set; }
        //List<Warehouse> WHINFO = new List<Warehouse>();
        public  Warehouse Warehouse { get; set; }
        public Item getItem { get; set; }
        public Vendor getVendor { get; set; }
        public Warehouse getWarehouse { get; set; }
        public List<Item> Item { get; }
    }
}
