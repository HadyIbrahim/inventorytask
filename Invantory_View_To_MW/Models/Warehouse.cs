namespace Invantory_View_To_MW.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("Warehouse")]
    public partial class Warehouse
    {

        [DisplayName("Warehouse_ID")]
        [Key]
        public int W_ID { get; set; }
        [DisplayName("Warehouse_Name")]
        [StringLength(50)]
        public string W_Name { get; set; }
        [DisplayName("Warehouse_UserID")]
        public string User_ID { get; set; }
        [DisplayName("Warehouse_Date")]
        [Column(TypeName = "date")]
        public DateTime? W_Date { get; set; }
        [DisplayName("Warehouse_Time")]
        public TimeSpan? W_Time { get; set; }
   

        // public virtual ICollection<All_W_V_I> All_W_V_I { get; set; }
    }
}
