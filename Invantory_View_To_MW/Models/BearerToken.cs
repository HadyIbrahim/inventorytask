﻿using Microsoft.OData.Edm;
using Newtonsoft.Json;
using System;

namespace Invantory_View_To_MW.Models
{
    public class BearerToken
    {
        [JsonProperty("access_token")]
        public string access_token { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty(".issued")]
        public string Issued { get; set; }

        [JsonProperty(".expires")]
        public string Expires { get; set; }
    }
    public class G_etDateToDate
    {
        //string token, DateTime? SFrom,DateTime? STo
        public DateTime SFrom { get; set; }
        public DateTime STo { get; set; }
      
    }
}