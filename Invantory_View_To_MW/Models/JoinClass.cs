﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Invantory_View_To_MW.Models
{
    public class JoinClass
    {
        public All_W_V_I getAll_W_V_I { get; set; }
        public Item getItem { get; set; }
        public Vendor getVendor { get; set; }
        public Warehouse getWarehouse { get; set; }
    }



    public partial class JoinTable
    {
        public int A_ID { get; set; }
        public string I_Name { get; set; }
        public string V_Name { get; set; }
        public string W_Name { get; set; }
        public int Qty { get; set; }
        public int? User_ID { get; set; }
        public DateTime? A_Date { get; set; }
        public TimeSpan? A_Time { get; set; }
    }
    public static class ApplicationInfo
    {
        public static string acctok { get; set; }

    }
}