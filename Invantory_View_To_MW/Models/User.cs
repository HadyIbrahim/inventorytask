﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Invantory_View_To_MW.Models
{
    public class User
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
      //  public String grant_type { get; set; }

        public string[] Roles { get; set; } 
        public String User_ID { get; set; }
        public String username { get; set; }
    }

}