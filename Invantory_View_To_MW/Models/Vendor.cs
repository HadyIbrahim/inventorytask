namespace Invantory_View_To_MW.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using System.Web.Mvc;

    [Table("Vendor")]
    public partial class Vendor
    {
        [DisplayName("Vendor_ID")]
        [Key]
        public int V_ID { get; set; }
        [DisplayName("Vendor_Name")]
        [StringLength(50)]
        public string V_Name { get; set; }
        [DisplayName("Vendor_Address")]
        [StringLength(50)]
        public string V_Address { get; set; }
        [DisplayName("Vendor_UserID")]
        public string User_ID { get; set; }
        [DisplayName("Vendor_Date")]
        [Column(TypeName = "date")]
        public DateTime? V_Date { get; set; }
        [DisplayName("Vendor_Time")]
        public TimeSpan? V_Time { get; set; }
    
      //  public SelectList Vendor { get; internal set; }
        //  public virtual ICollection<All_W_V_I> All_W_V_I { get; set; }
    }
}
