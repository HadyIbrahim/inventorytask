﻿using Invantory_View_To_MW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Invantory_View_To_MW.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index1()
        {
            ViewBag.Message = "Multiple DropDown List";
            Detail obj = new Detail();

            List<Item> obj1 = new List<Item>();
          
            //obj1 = Index();
            SelectList objlistofcountrytobind = new SelectList(obj1, "I_ID", "I_Name", 0);
          
           // obj.ItemList = objlistofcountrytobind;
       
            return View(obj);
        }
        //public async Task<ActionResult> Index(string token)
        //{

        //    List<Item> objEmp = new List<Item>();
        //    if (token == null)
        //    {
        //        token = ApplicationInfo.acctok;
        //    }
        //    ApplicationInfo.acctok = token;
        //    IEnumerable<Item> items = null;
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("https://localhost:44379/");
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        //        var result = await client.GetAsync("api/Items");
        //        if (result.IsSuccessStatusCode)
        //        {
                
        //            items = await result.Content.ReadAsAsync<IList<Item>>();
        //            items = objEmp;
        //        }
        //        else
        //        {
        //            items = Enumerable.Empty<Item>();
        //            ModelState.AddModelError(string.Empty, "Server error try after some time.");
        //        }
        //    }
        //    return View(items);
        //}
        private readonly IUserSession _userSession;

        public HomeController(IUserSession userSession)
        {
            _userSession = userSession;
        }

        // GET: Home
        //public ActionResult Index()
        //{

        //    ViewBag.EmailAddress = _userSession.Username;
        //    ViewBag.AccessToken = _userSession.BearerToken;

        //    return View();
        //}
    }


    public interface IUserSession
    {
        string Username { get; }
        string BearerToken { get; }
    }

    public class UserSession : IUserSession
    {

        public string Username
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst(ClaimTypes.Name).Value; }
        }

        public string BearerToken
        {
            get { return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst("AcessToken").Value; }
        }
        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return View();
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
        //public ActionResult Login()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
        
    }
}