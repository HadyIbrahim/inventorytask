﻿using DocumentFormat.OpenXml.Office.CustomUI;
using Invantory_View_To_MW.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using warehouses = Invantory_View_To_MW.Models.Warehouse;

namespace Invantory_View_To_MW.Controllers
{

    public class WarehouseController : Controller
    {
        // GET: Warehouse
        // GET: Vendor
        public async Task<ActionResult> Index(string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            IEnumerable<Warehouse> warehouses = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync("api/Warehouses");

                if (result.IsSuccessStatusCode)
                {


                    warehouses = await result.Content.ReadAsAsync<IList<warehouses>>();
                }
                else
                {
                    warehouses = Enumerable.Empty<warehouses>();
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }
            return View(warehouses);
        }

        public async Task<ActionResult> Details(string id,string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Warehouse Warehouses = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync($"api/Warehouses?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    Warehouses = await result.Content.ReadAsAsync<Warehouse>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }

            if (Warehouses == null)
            {
                return HttpNotFound();
            }
            return View(Warehouses);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Warehouse employee,string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://localhost:44363/api/Warehouses");
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = await client.PostAsJsonAsync("Warehouses", employee);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Server error try after some time.");
                    }
                }
            }
            return View(employee);
        }

        public async Task<ActionResult> Edit(string id,string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Warehouse employee = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/Warehouses/");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync($"Warehouses?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    employee = await result.Content.ReadAsAsync<Warehouse>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Warehouse employee,string token)
        {

            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);

                    client.BaseAddress = new Uri("https://localhost:44363/api/Warehouses");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = await client.PutAsJsonAsync("Warehouses", employee);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Server error try after some time.");
                    }
                }
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        public async Task<ActionResult> Delete(string id,string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Warehouse employee = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync($"Warehouses?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    employee = await result.Content.ReadAsAsync<Warehouse>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }

            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id,string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await client.DeleteAsync($"Warehouses?id={id}");
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
            }
            return View();
        }
    }
}