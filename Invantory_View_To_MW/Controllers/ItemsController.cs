﻿using DocumentFormat.OpenXml.Office.CustomUI;
using Invantory_View_To_MW.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using Item = Invantory_View_To_MW.Models.Item;

namespace Invantory_View_To_MW.Controllers
{
 
    public class ItemsController : Controller
    {
        public async Task<ActionResult> Index(string token)
        {
         
            if (token==null)
            {
                 token = ApplicationInfo.acctok ;
            }
            ApplicationInfo.acctok = token;

            IEnumerable<Item> items = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",token);
                var result = await client.GetAsync("api/Items");
                if (result.IsSuccessStatusCode)
                {
                    items = await result.Content.ReadAsAsync<IList<Item>>();
                }
                else
                {
                    items = Enumerable.Empty<Item>();
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }
            return View(items);
        }
        public List<Item> GetAllStates(string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            var list = new List<Item>();
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response;
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            response = httpClient.GetAsync("https://localhost:44363/api/Items").Result;
            response.EnsureSuccessStatusCode();
            List<Item> stateList = response.Content.ReadAsAsync<List<Item>>().Result;
            if (!object.Equals(stateList, null))
            {
                var states = stateList.ToList();
                return states;
            }
            else
            {
                return null;
            }
        }
        public async Task<ActionResult> Details(string token , string id)
           
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Item items = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync($"api/Items?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    items = await result.Content.ReadAsAsync<Item>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }

            if (items == null)
            {
                return HttpNotFound();
            }
            return View(items);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(string token,Item employee)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://localhost:44363/api/Items");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = await client.PostAsJsonAsync("Items", employee);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Server error try after some time.");
                    }
                }
            }
            return View(employee);
        }

        public async Task<ActionResult> Edit(string token,string id)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item employee = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/Items");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync($"Items?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    employee = await result.Content.ReadAsAsync<Item>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(string token,Item employee)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://localhost:44363/api/Items");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = await client.PutAsJsonAsync("Items", employee);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Server error try after some time.");
                    }
                }
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        public async Task<ActionResult> Delete(string token,string id)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item employee = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync($"Items?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    employee = await result.Content.ReadAsAsync<Item>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }

            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string token,string id)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await client.DeleteAsync($"Items?id={id}");
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
            }
            return View();
        }

    }
}