﻿using Invantory_View_To_MW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Invantory_View_To_MW.Controllers
{
    public class WarehousesController : Controller
    {
       
        [HttpGet]
        public ActionResult Index()
        {
            IEnumerable<Warehouse> Warehouses = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Warehouses");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Warehouse>>();
                    readTask.Wait();

                    Warehouses = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    Warehouses = System.Linq.Enumerable.Empty<Warehouse>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(Warehouses);
        }

        public ActionResult Create()
        {
            return View();
        }
        // POST: Vendors/Create
        [HttpPost]
        public ActionResult Create(Warehouse whr)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Warehouse>("Warehouses", whr);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(whr);
        }

        public ActionResult Edit()
        {
            return View();
        }
        // Put: Vendors/Edit/5
        [HttpPut]
        public ActionResult Edit(int id)
        {
            Warehouse Warehouses = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Warehouses?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Warehouse>();
                    readTask.Wait();

                    Warehouses = readTask.Result;
                }
            }

            return View(Warehouses);
        }


        public ActionResult Details()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            IEnumerable<Warehouse> Warehouses = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Warehouses?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Warehouse>>();
                    readTask.Wait();

                    Warehouses = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    Warehouses = System.Linq.Enumerable.Empty<Warehouse>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(Warehouses);
        }
        public ActionResult Delete()
        {
            return View();
        }
        // POST: Vendors/Edit/5
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Warehouses?id=" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    }
}
