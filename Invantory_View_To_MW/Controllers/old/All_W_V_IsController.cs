﻿using Invantory_View_To_MW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Invantory_View_To_MW.Controllers
{
    public class All_W_V_IsController : Controller
    {
        public ActionResult Index(int x)
        {
            return View();
        }
        [HttpGet]
        public ActionResult Index()
        {
            IEnumerable<All_W_V_I> All_W_V_Is = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");
                //HTTP GET
                var responseTask = client.GetAsync("All_W_V_I");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<All_W_V_I>>();
                    readTask.Wait();

                    All_W_V_Is = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    All_W_V_Is = System.Linq.Enumerable.Empty<All_W_V_I>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(All_W_V_Is);
        }

        public ActionResult Create()
        {
            return View();
        }
        // POST: Vendors/Create
        [HttpPost]
        public ActionResult Create(All_W_V_I All_W_V_Is)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<All_W_V_I>("All_W_V_I", All_W_V_Is);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(All_W_V_Is);
        }

        public ActionResult Edit()
        {
            return View();
        }
        // Put: Vendors/Edit/5
        [HttpPut]
        public ActionResult Edit(int id)
        {
            All_W_V_I All_W_V_Is = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");
                //HTTP GET
                var responseTask = client.GetAsync("All_W_V_I?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<All_W_V_I>();
                    readTask.Wait();

                    All_W_V_Is = readTask.Result;
                }
            }

            return View(All_W_V_Is);
        }


        public ActionResult Details()
        {
            return View();
        }

      

        [HttpGet]
        public ActionResult Details(int id)
        {
            IEnumerable<All_W_V_I> All_W_V_Is = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");
                //HTTP GET
                var responseTask = client.GetAsync("All_W_V_I?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<All_W_V_I>>();
                    readTask.Wait();

                    All_W_V_Is = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    All_W_V_Is = System.Linq.Enumerable.Empty<All_W_V_I>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(All_W_V_Is);
        }
        public ActionResult Delete()
        {
            return View();
        }
        // POST: Vendors/Edit/5
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("All_W_V_I?id=" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    }
}
