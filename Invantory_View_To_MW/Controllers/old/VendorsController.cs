﻿using Invantory_View_To_MW.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Mvc;
namespace Invantory_View_To_MW.Controllers
{
    public class VendorsController : Controller
    {
        // GET: Vendors
        //public ActionResult Index()
        //{
        //    return View();
        //}
        [HttpGet]
    public ActionResult Index()
    {
        IEnumerable<Vendor> Vendors = null;

        using (var client = new HttpClient())
        {
            client.BaseAddress = new Uri("https://localhost:44379/api/");
            //HTTP GET
            var responseTask = client.GetAsync("Vendors");
            responseTask.Wait();

            var result = responseTask.Result;
            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<IList<Vendor>>();
                readTask.Wait();

                    Vendors = readTask.Result;
            }
            else //web api sent error response 
            {
                    //log response status here..

                    Vendors = System.Linq.Enumerable.Empty<Vendor>();

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            }
        }
        return View(Vendors);
    }

   
        // POST: Vendors/Create
        [HttpPost]
        public ActionResult Create(Vendor Vendors)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Vendor>("Vendors", Vendors);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(Vendors);
        }


        // Put: Vendors/Edit/5
    [HttpPut]
        public ActionResult Edit(int id)
        {
            Vendor Vendors = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Vendors?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Vendor>();
                    readTask.Wait();

                    Vendors = readTask.Result;
                }
            }

            return View(Vendors);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            IEnumerable<Vendor> Vendors = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Vendors?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Vendor>>();
                    readTask.Wait();

                    Vendors = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    Vendors = System.Linq.Enumerable.Empty<Vendor>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(Vendors);
        }

        // POST: Vendors/Edit/5
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Vendors?id=" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    }
}
