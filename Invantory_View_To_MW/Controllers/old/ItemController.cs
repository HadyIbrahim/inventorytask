﻿using Invantory_View_To_MW.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Invantory_View_To_MW.Controllers
{
    public class ItemController : Controller
    {

    
        string Baseurl = "https://localhost:44379/";
        public async Task<ActionResult> Index()
        {
            List<Item> EmpInfo = new List<Item>();
            using (var client = new HttpClient())
            {
                //Passing service base url
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //Sending request to find web api REST service resource GetAllEmployees using HttpClient
                HttpResponseMessage Res = await client.GetAsync("api/Items");
                //Checking the response is successful or not which is sent using HttpClient
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    //Deserializing the response recieved from web api and storing into the Employee list
                    EmpInfo = JsonConvert.DeserializeObject<List<Item>>(EmpResponse);
                }
                //returning the employee list to view
                return View(EmpInfo);
            }
        }
        //public ActionResult Index()
        //{
        //    IEnumerable<Item> Items = null;

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("https://localhost:44379/api/");
        //        //HTTP GET
        //        var responseTask = client.GetAsync("Items");
        //        responseTask.Wait();

        //        var result = responseTask.Result;
        //        if (result.IsSuccessStatusCode)
        //        {
        //            var readTask = result.Content.ReadAsAsync<IList<Item>>();
        //            readTask.Wait();

        //            Items = readTask.Result;
        //        }
        //        else //web api sent error response 
        //        {
        //            //log response status here..

        //            Items = System.Linq.Enumerable.Empty<Item>();

        //            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
        //        }
        //    }
        //    return View(Items);
        //}

      
            // POST: Vendors/Create
            [HttpPost]
        public ActionResult Create(Item Items)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/Items");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Item>("Items", Items);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(Items);
        }


        //public ActionResult Edit(int id)
        //{
        //    Item Items = null;

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("https://localhost:44379/api/Items?id");
        //        //HTTP GET
        //        var responseTask = client.GetAsync("Items?id=" + id.ToString());

        //        responseTask.Wait();

        //        var result = responseTask.Result;
        //        if (result.IsSuccessStatusCode)
        //        {
        //            var readTask = result.Content.ReadAsAsync<Item>();
        //            readTask.Wait();

        //            Items = readTask.Result;
        //        }
        //    }

        //    return View(Items);
        //}

        public ActionResult Edit(int id)
        {
            Item ItemsXD = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");
                //HTTP GET
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/Json"));
                var responseTask = client.GetAsync("Items?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Item>();
                    readTask.Wait();

                    ItemsXD = readTask.Result;

                }
            }
            return View(ItemsXD);
        }
        [HttpPost]
        public ActionResult Edit(Item Items)
        {
            using (var client = new HttpClient())
            {
              //  client.BaseAddress = new Uri("http://localhost:64189/api/student");
                client.BaseAddress = new Uri("https://localhost:44379/api/Items");

                //HTTP POST
                var putTask = client.PutAsJsonAsync<Item>("Items", Items);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return View(Items);
        }


        [HttpGet]
        public ActionResult Details(int id)
        {
            IEnumerable<Item> Items = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Items/id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Item>>();
                    readTask.Wait();

                    Items = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    Items = System.Linq.Enumerable.Empty<Item>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(Items);
        }
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44379/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Items?id=" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    
    
    
    
    }


}
