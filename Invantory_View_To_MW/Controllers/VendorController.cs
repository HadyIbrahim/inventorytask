﻿using DocumentFormat.OpenXml.Office.CustomUI;
using Invantory_View_To_MW.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using Vendor = Invantory_View_To_MW.Models.Vendor;

namespace Invantory_View_To_MW.Controllers
{

    public class VendorController : Controller
    {
        // GET: Vendor
        public async Task<ActionResult> Index(string token)
        {

            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;


            IEnumerable<Vendor> vendors = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync("api/Vendors");

                if (result.IsSuccessStatusCode)
                {
                    vendors = await result.Content.ReadAsAsync<IList<Vendor>>();
                }
                else
                {
                    vendors = Enumerable.Empty<Vendor>();
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }
            return View(vendors);
        }

        public async Task<ActionResult> Details(string id,string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Vendor vendors = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync($"api/Vendors?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    vendors = await result.Content.ReadAsAsync<Vendor>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }

            if (vendors == null)
            {
                return HttpNotFound();
            }
            return View(vendors);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Vendor employee,String token)
        {

            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://localhost:44363/api/Vendors");
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = await client.PostAsJsonAsync("Vendors", employee);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Server error try after some time.");
                    }
                }
            }
            return View(employee);
        }

        public async Task<ActionResult> Edit(string id,string token)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            Vendor employee = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/Vendors");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync($"Vendors?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    employee = await result.Content.ReadAsAsync<Vendor>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Vendor employee,string token)
        {

            if (ModelState.IsValid)
            {
                token = ApplicationInfo.acctok;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://localhost:44363/api/Vendors");
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = await client.PutAsJsonAsync("Vendors", employee);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Server error try after some time.");
                    }
                }
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        public async Task<ActionResult> Delete(string id,string token)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            Vendor employee = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = await client.GetAsync($"Vendors?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    employee = await result.Content.ReadAsAsync<Vendor>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }

            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id,string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await client.DeleteAsync($"Vendors?id={id}");
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
            }
            return View();
        }
    }
}