﻿using DocumentFormat.OpenXml.Office.CustomUI;
using Invantory_View_To_MW.Models;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Token = Invantory_View_To_MW.Models.Token;

namespace Invantory_View_To_MW.Controllers
{
    public class TokenController : Controller
    {


        public ActionResult Create()

        {

            return View();
        }
        //api/Token/Create
        //    public ActionResult Create(User model)
        //{
        //    var getTokenUrl = "https://localhost:44379/token";

        //    using (HttpClient httpClient = new HttpClient())
        //    {
        //        HttpContent content = new FormUrlEncodedContent(new[]
        //        {
        //            new KeyValuePair<string, string>("grant_type", "password"),
        //            new KeyValuePair<string, string>("username", model.username),
        //            new KeyValuePair<string, string>("password", model.Password)
        //        });

        //        HttpResponseMessage result = httpClient.PostAsync(getTokenUrl, content).Result;

        //        string resultContent = result.Content.ReadAsStringAsync().Result;

        //        var token = JsonConvert.DeserializeObject<Token>(resultContent);

        //        AuthenticationProperties options = new AuthenticationProperties();

        //        options.AllowRefresh = true;
        //        options.IsPersistent = true;
        //        options.ExpiresUtc = DateTime.UtcNow.AddSeconds(int.Parse(token.expires_in));

        //        var claims = new[]
        //        {
        //            new Claim(ClaimTypes.Name, model.username),
        //            new Claim("AcessToken", string.Format("Bearer {0}", token.access_token)),
        //        };

        //        var identity = new ClaimsIdentity(claims, "ApplicationCookie");

        //      //  Request.GetOwinContext().Authentication.SignIn(options, identity);

        //    }

        //    return RedirectToAction("Index", "Home");
        //}

        //private static List<Token> Create(string username, string password, string grant_type)
        //{
        //    List<Token> Tokens = new List<Token>();
        //    string apiUrl = "https://localhost:44379/";

        //    HttpClient client = new HttpClient();
        //    HttpResponseMessage response = client.GetAsync(apiUrl + string.Format("/token")).Result;
        //    //?username={0}", username, password, grant_type
        //    if (response.IsSuccessStatusCode)
        //    {
        //        Tokens = (new JavaScriptSerializer()).Deserialize<List<Token>>(response.Content.ReadAsStringAsync().Result);
        //    }

        //    return Tokens;
        //}
        //// GET: Token/Edit/5
        /////
        public ActionResult Login()
        {
            //  return Redirect(Url.Content("~/index.html"));S
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create(Token model)
        {
            var getTokenUrl = "https://localhost:44363/token";
            using (HttpClient httpClient = new HttpClient())
            {
                HttpContent content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", model.username),
                    new KeyValuePair<string, string>("password", model.password)
                });
              
                HttpResponseMessage result = httpClient.PostAsync(getTokenUrl, content).Result;
                string raw_API = JsonConvert.SerializeObject(model, Formatting.Indented);
                Write_Log_ToFile(raw_API);
                string resultContent = result.Content.ReadAsStringAsync().Result;
                var token = JsonConvert.DeserializeObject<BearerToken>(resultContent);
                AuthenticationProperties options = new AuthenticationProperties();
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, model.username),
                    new Claim("AcessToken", string.Format("Bearer {0}", token.access_token)),
                };
                var identity = new ClaimsIdentity(claims, "ApplicationCookie");
                string accesstoken01 = token.access_token;
                model.access_token = accesstoken01;
            }
            return RedirectToAction("Index", "Items", new {token = model.access_token});
        }


        private void Write_Log_ToFile(string json)
        {
            string filename = String.Format("{0}", DateTime.Now.ToString("ddMMyyyy_HHmmss")) + ".txt"; ;
            string path = Path.Combine(@"E:\Inventory_LOG", filename);
            System.IO.File.WriteAllText(path, json);
        }
        //public async Task<ActionResult> Create(string username, string password)
        //{
        //    var accessToken = string.Empty;
        //    if (ModelState.IsValid)
        //    {
        //        using (var client = new HttpClient())
        //        {
        //         client.BaseAddress = new Uri("https://localhost:44379/");
        //         // string url= "https://localhost:44379/";
        //            var KeyValue = new List<KeyValuePair<string, string>>
        //            {
        //            new KeyValuePair<string, string>("username",username),
        //            new KeyValuePair<string, string>("password",password),
        //            new KeyValuePair<string, string>("grant_type","password")
        //        };


        //          //  HttpResponseMessage responseMessage = await client.PostAsync("/Token", formContent);

        //            //        //get access token from response body
        //            //        var responseJson = await responseMessage.Content.ReadAsStringAsync();

        //            //        var jObject = JObject.Parse(responseJson);

        //            var responsex = await client.PostAsJsonAsync("token?", KeyValue);

        //            responsex.Content = new FormUrlEncodedContent(KeyValue);

        //            //       var response3 = client.SendAsync(responsex).Result;
        //            using (HttpContent content = responsex.Content)
        //            {
        //                var json = content.ReadAsStringAsync();
        //                JObject jwtDynamic = JsonConvert.DeserializeObject<dynamic>(json.Result);
        //                var accesstokenExpiration = jwtDynamic.Value<DateTime>(".expires");
        //                accessToken = jwtDynamic.Value<string>("access_token");
        //                var username2 = jwtDynamic.Value<string>("username");
        //                var AccessTokenExpirationade = accesstokenExpiration;

        //                if (responsex.IsSuccessStatusCode)
        //                {
        //                    return RedirectToAction("Index?token=" + accessToken);
        //                }
        //                else
        //                {
        //                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
        //                }
        //            }
        //        }

        //    }
        //    return View("Items/Index?token=" + accessToken);
        //}
        //return View("Index?token=" + accessToken);

        //     }



        //[HttpPost]
        //[ValidateAntiForgeryToken]
        ////public async Task<ActionResult> Create(Token token)
        ////{

        //    if (ModelState.IsValid)
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri("https://localhost:44379");
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //            //setup login data
        //            var formContent = new FormUrlEncodedContent(new[] {

        //            new KeyValuePair<string, string>("grant_type", "password"),
        //            new KeyValuePair<string, string>("username", token.username),
        //            new KeyValuePair<string, string>("password", token.password),

        //            });
        //            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
        //            var response = await client.PostAsJsonAsync("/token", formContent);
        //            var responseJson = await response.Content.ReadAsStringAsync();
        //            var jObject = JObject.Parse(responseJson);

        //            if (response.IsSuccessStatusCode)
        //            {
        //                string TOKey = jObject.GetValue("access_token").ToString();
        //                return RedirectToAction("~/Items/Index?Token="+ TOKey);
        //            }
        //            else
        //            {
        //                ModelState.AddModelError(string.Empty, "Server error try after some time.");
        //            }
        //        }
        //    }
        //    return View(token);
        //}


        //public IActionResult PostItem(string username,string password)
        //{
        //    var url = "https://localhost:44379";
        //    var accessToken = string.Empty;
        //        var client = new HttpClient();
        //    try
        //    {
        //        var KeyValue = new List<KeyValuePair<string, string>>
        //            {
        //            new KeyValuePair<string, string>("username",username),
        //            new KeyValuePair<string, string>("password",password),
        //            new KeyValuePair<string, string>("grant_type","password")
        //        };
        //        var response2 =  client.PostAsJsonAsync(url+"token", KeyValue);
        //        //var requst = new HttpRequestMessage(
        //        //    HttpMethod.Post, url, "/token");

        //        response2.Content = new FormUrlEncodedContent(KeyValue);

        //        var response = client.SendAsync(response2).Result;
        //        using (HttpContent content = response.Content)
        //        {
        //            var json = content.ReadAsStringAsync();
        //            JObject jwtDynamic = JsonConvert.DeserializeObject<dynamic>(json.Result);
        //            var accesstokenExpiration = jwtDynamic.Value<DateTime>(".expires");
        //            accessToken = jwtDynamic.Value<string>("access_token");
        //            var username2 = jwtDynamic.Value<string>("username");
        //            var AccessTokenExpirationade = accesstokenExpiration;
        //         //   Response.Redirect();

        //        }

        //    }

        //    return View("/index?token=" + accessToken);
        //}

        //    return accessToken;
        //}
        //public async Task<string> GetAPIToken(string userName, string password)
        //{
        //    using (var client = new HttpClient())
        //    {
        //        //setup client
        //        client.BaseAddress = new Uri("https://localhost:44379");
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        //setup login data
        //        var formContent = new FormUrlEncodedContent(new[]
        //        {
        //            new KeyValuePair<string, string>("grant_type", "password"),
        //            new KeyValuePair<string, string>("username", userName),
        //            new KeyValuePair<string, string>("password", password),
        //        });

        //        //send request
        //        HttpResponseMessage responseMessage = await client.PostAsync("/Token", formContent);

        //        //get access token from response body
        //        var responseJson = await responseMessage.Content.ReadAsStringAsync();
        //        var jObject = JObject.Parse(responseJson);
        //      //  var TOKey = jObject.GetValue("access_token").ToString();

        //        string TOKey = jObject.GetValue("access_token").ToString();

        //        return ok("~/Items/Index");

        //    }
        //}


        //private  async Task<string> GetAPIToken(Token token)
        //{
        //    using (var client = new HttpClient())
        //    {

        //        StringContent stringContent = new StringContent(JsonConvert.SerializeObject(token), Encoding.UTF8, "application/json");
        //        using (var response=await client.PostAsync("https://localhost:44379/Token"))
        //        {
        //            string responseJson = await response.Content.ReadAsStringAsync();
        //            //if (responseJson=="Invalid credentials")
        //            //{
        //            //    ViewBag.Message = "Incorrent UserID or Password!";
        //            //    return Redirect("~/Home/Login");
        //            //}
        //            HttpContext.Session.SetString("bearer", responseJson);

        //        }
        //        return Redirect("~/Items/Index");
        //    }
        //}
    }
    }

