﻿using DocumentFormat.OpenXml.Office.CustomUI;
using Invantory_View_To_MW.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using All_W_V_I = Invantory_View_To_MW.Models.All_W_V_I;
using Item = Invantory_View_To_MW.Models.Item;

namespace Invantory_View_To_MW.Controllers
{

    public class All_W_V_IController : Controller
    {
        //public async Task<ActionResult> Index(string token)
        //{


        //    if (token == null)
        //    {
        //        token = ApplicationInfo.acctok;
        //    }
        //    ApplicationInfo.acctok = token;

        //    List<JoinClass> JC = new List<JoinClass>();
        //    using (var client = new HttpClient())
        //    {
        //        //Passing service base url
        //        client.BaseAddress = new Uri("https://localhost:44363/");
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        //        client.DefaultRequestHeaders.Clear();
        //        //Define request data format
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        //Sending request to find web api REST service resource GetAllEmployees using HttpClient
        //        HttpResponseMessage Res = await client.GetAsync("api/JoinAllTables/JoinAllTables");
        //        //Checking the response is successful or not which is sent using HttpClient
        //        if (Res.IsSuccessStatusCode)
        //        {
        //            //Storing the response details recieved from web api
        //            var EmpResponse = Res.Content.ReadAsStringAsync().Result;
        //            //Deserializing the response recieved from web api and storing into the Employee list
        //            JC = JsonConvert.DeserializeObject<List<JoinClass>>(EmpResponse);
        //        }
        //        //returning the employee list to view
        //        return View(JC);
        //    }
        //}

        public ActionResult Index(string token)
        {


            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            IEnumerable<JoinClass> JC2 = null;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44363/");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);

            var consumAPI = client.GetAsync("api/JoinAllTables/JoinAllTables");

            consumAPI.Wait();
            var readdata = consumAPI.Result;


            if (readdata.IsSuccessStatusCode)
            {
                var Desplaydata2 = readdata.Content.ReadAsAsync<List<JoinClass>>();
                Desplaydata2.Wait();
                JC2 = Desplaydata2.Result;

                //all_W_V_Is = await result.Content.ReadAsAsync<IList<All_W_V_I>>();
            }


            return View(JC2);
        }

        //public IEnumerable<Warehouse> GetMobileList()
        //{
        //    IEnumerable<JoinClass> JC = null;
        //    HttpClient client = new HttpClient();
        //    client.BaseAddress = new Uri("https://localhost:44379/");
        //    var consumAPI = client.GetAsync("api/AllListSys/getDDLLIST");


        //    consumAPI.Wait();
        //    var readdata = consumAPI.Result;


        //    if (readdata.IsSuccessStatusCode)
        //    {
        //        var Desplaydata = readdata.Content.ReadAsAsync<List<JoinClass>>();
        //        Desplaydata.Wait();
        //        JC = Desplaydata.Result;

        //        //all_W_V_Is = await result.Content.ReadAsAsync<IList<All_W_V_I>>();
        //    }


        //    return View(JC);
        //}

    
        public async Task<ActionResult> Index2(string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;
            IEnumerable<All_W_V_I> all_W_V_Is = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);

                var result = await client.GetAsync("api/All_W_V_I");

                if (result.IsSuccessStatusCode)
                {
                    all_W_V_Is = await result.Content.ReadAsAsync<IList<All_W_V_I>>();
                }
                else
                {
                    all_W_V_Is = Enumerable.Empty<All_W_V_I>();
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }
            return View(all_W_V_Is);
        }


        public async Task<ActionResult> JoinTable(string token)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;
            IEnumerable<JoinTable> JoinTables = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);

                var result = await client.GetAsync("api/JoinTables");

                if (result.IsSuccessStatusCode)
                {
                    JoinTables = await result.Content.ReadAsAsync<IList<JoinTable>>();
                }
                else
                {
                    JoinTables = Enumerable.Empty<JoinTable>();
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }
            return View(JoinTables);
        }

        //public async Task<ActionResult> Index1(string token, DateTime? SFrom, DateTime? STo)
        //{
        //    if (token == null)
        //    {
        //        token = ApplicationInfo.acctok;
        //    }
        //    ApplicationInfo.acctok = token;
        //    IEnumerable<All_W_V_I> all_W_V_Is = null;

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("https://localhost:44379/api/All_W_V_I/");
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        //        // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);

        //        var result = await client.GetAsync("Sereachdate?StartDate=" + SFrom + "&&EndDate=" + STo);

        //        if (result.IsSuccessStatusCode)
        //        {
        //            all_W_V_Is = await result.Content.ReadAsAsync<IList<All_W_V_I>>();
        //        }
        //        else
        //        {
        //            all_W_V_Is = Enumerable.Empty<All_W_V_I>();
        //            ModelState.AddModelError(string.Empty, "Server error try after some time.");
        //        }
        //    }
        //    return View(all_W_V_Is);
        //}

        public  ActionResult Index1(string token, DateTime? SFrom,DateTime? STo)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;

            IEnumerable<All_W_V_I> all_W_V_Is = null;

            HttpClient hc = new HttpClient();
       
                hc.BaseAddress = new Uri("https://localhost:44363/api/All_W_V_I/");
            hc.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);

            var dataa = hc.GetAsync("Sereachdate?StartDate="+SFrom+"&&EndDate="+STo);
            dataa.Wait();
            var ReadData = dataa.Result;

                if (ReadData.IsSuccessStatusCode)
                {

                var DesplayRecord = ReadData.Content.ReadAsAsync<IList<All_W_V_I>>();
                //all_W_V_Is = JsonConvert.DeserializeObject<All_W_V_I>(DesplayRecord);
                DesplayRecord.Wait();
                all_W_V_Is = DesplayRecord.Result;
                
            }

            else
            {
                all_W_V_Is = Enumerable.Empty<All_W_V_I>();
                ModelState.AddModelError(string.Empty,"No Records Found.............!");
            }
            return View(all_W_V_Is);
        }
        public async Task<ActionResult> Details(string token, string id)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            All_W_V_I all_W_V_Is = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                var result = await client.GetAsync($"api/All_W_V_I?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    all_W_V_Is = await result.Content.ReadAsAsync<All_W_V_I>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }

            if (all_W_V_Is == null)
            {
                return HttpNotFound();
            }
            return View(all_W_V_Is);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(string token, All_W_V_I employee)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://localhost:44363/api/All_W_V_I");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                    var response = await client.PostAsJsonAsync("All_W_V_I", employee);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("JoinTables");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Server error try after some time.");
                    }
                }
            }
            return View(employee);
        }

        public async Task<ActionResult> Edit(string token, string id)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            All_W_V_I employee = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/All_W_V_I");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                var result = await client.GetAsync($"All_W_V_I?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    

                    employee = await result.Content.ReadAsAsync<All_W_V_I>();
                    //employee datalist = JsonConvert.DeserializeObject<employee>(jsonstring);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(string token, All_W_V_I employee)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://localhost:44363/api/All_W_V_I");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                    var response = await client.PutAsJsonAsync("All_W_V_I", employee);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("JoinTables");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Server error try after some time.");
                    }
                }
                return RedirectToAction("JoinTables");
            }
            return View(employee);
        }

        public async Task<ActionResult> Delete(string token, string id)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            All_W_V_I employee = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                var result = await client.GetAsync($"All_W_V_I?id={id}");

                if (result.IsSuccessStatusCode)
                {
                    employee = await result.Content.ReadAsAsync<All_W_V_I>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
                }
            }

            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string token,string id)
        {
            if (token == null)
            {
                token = ApplicationInfo.acctok;
            }
            ApplicationInfo.acctok = token;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44363/api/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.AccessToken);
                var response = await client.DeleteAsync($"All_W_V_I?id={id}");
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("JoinTables");
                }
                else
                    ModelState.AddModelError(string.Empty, "Server error try after some time.");
            }
            return View();
        }
    }
}