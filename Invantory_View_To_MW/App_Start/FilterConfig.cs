﻿using System.Web;
using System.Web.Mvc;

namespace Invantory_View_To_MW
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
